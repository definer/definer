DEFINER
=======

Marco Antoniotti  
See file COPYING for licensing information


DESCRIPTION
-----------

The DEFINER library adds a simple macro DEF to Common Lisp that
replaces the various 'def*' forms in the language.  It is a simple
hack, but it adds some elegance to the language.  Of course, it comes
with its own way to be extended.


A NOTE ON FORKING
-----------------

Of course you are free to fork the project subject to the current
licensing scheme.  However, before you do so, I ask you to consider
plain old "cooperation" by asking me to become a developer.
It helps keeping the entropy at an acceptable level.


Enjoy.
