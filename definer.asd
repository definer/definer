;;;; -*- Mode: Lisp -*-

;;;; definer.asd --
;;;;
;;;; See the file COPYING for license and copying information.

(asdf:defsystem #:definer
  :author "Marco Antoniotti"
  :license "BSD"
  :components ((:file "definer-pkg")
               (:file "definer" :depends-on ("definer-pkg")))

  :description "A DEF macro for Common Lisp.

The DEFINER library adds a simple macro DEF to Common Lisp that
replaces the various 'def*' forms in the language.  It is a simple
hack, but it adds some elegance to the language.  Of course, it comes
with its own way to be extended."
)


;;;; end of file -- definer.asd --
