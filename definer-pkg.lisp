;;; -*- Mode: Lisp -*-

;;; definer-pkg.lisp --

;;; See the file COPYING for copyright and licensing information.

(defpackage "IT.UNIMIB.DISCO.MA.CL.EXT.SYNTAX.DEFINER" (:use "COMMON-LISP")
  (:nicknames  "CL.EXT.SYNTAX.DEFINER" "DEFINER" #-lispworks "DEF" "DEFN")

  (:documentation "The CL Extensions Definer Package.

The package contains a simple extension to Common Lisp by introducing
a simple DEF macro.

Notes:

The package name is long because it indicates how to position the
library functionality within the breakdown in chapters of the ANSI
specification.

The \"CL.EXT.SYNTAX.DEFINER\" nickname is provided as a suggestion
about how to 'standardize' package names according to a meaninguful
scheme.")

  (:export
   "DEF"
   "BUILD-DEFINITION-FORM")

  (:export ; Names missing from the CL package.
   "VAR"
   "PARAMETER"
   "CONSTANT"

   "MACRO"
   "GENERIC"
   "METHOD"
   "METHOD-COMBINATION"

   "SETF-EXPANDER"
   "MODIFY-MACRO"

   "DEFINER"
   ))

;;; end of file -- definer-pkg.lisp --
