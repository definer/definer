;;;; -*- Mode: Lisp -*-

;;;; definer.lisp --
;;;; Many CL packages extend the language by adding a slew of
;;;;
;;;; (defsomething ...) and/or (define-something-else ...)
;;;;
;;;; macros.  Well, wouldn't it be nice to have a more controlled way
;;;; to do that?  DEFINER is for you. A quick hack where we extend CL
;;;; to define more complex definition forms.

;;;; See the file COPYING for copyright and licensing information.


(in-package "CL.EXT.SYNTAX.DEFINER")

;;;; build-definition-form

(defgeneric build-definition-form (what
                                   name
                                   definition-forms
                                   &optional prefix-options)
  (:documentation
   "Constructs the actual 'definition form' based on the arguments received.

The methods of this generic function are called by the DEF macro to
perform the eventual expansion of the definition.

The optional parameter PREFIX-options can be used to pass around extra
information - usually, but not necessarily, in the form of a list of
symbols - when WHAT, or NAME is a structured, non-symbol, object.

Arguments and Values:

WHAT : a T (actually a S-expr)
NAME : a SYMBOL
DEFINITION-FORMS : a FORM
PREFIX-OPTIONS : a T (actually a S-expr)
result : a FORM

Examples:

;;; A simple, straightforward (and useless?) example is the following:

(defmethod build-definition-form ((what (eql 'parameter))
                                  (name symbol)
                                  definition-forms
                                  &optional prefix-options)
  `(defparameter ,name ,@definition-forms))

;;; Given the definition above, now we can do:

cl-prompt> (def parameter *qd* 42 \"The answer.\")
*QD*
 
cl-prompt> *qd*
42
"))


;;;; Implementation
;;;; ======================================================================

;;; def --

(defmacro def (what name &rest definition-forms)
  "Expands into the appropriate definition form for WHAT.

The macro eventually calls BUILD-DEFINITION-FORM to construct the
proper definition form for NAME. WHAT directs the underlying form
builder code in a method of BUILD-DEFINITION-FORM to produce the
appropriate RESULT.

NAME can be processed to produce the actual 'name' of the object being
defined.

The actual contents of DEFINITION-FORMS depend on WHAT is being
defined.

Arguments and Values:

WHAT : a SYMBOL or a CONS
NAME : a SYMBOL or a CONS
DEFINITION-FORMS : a 'compound form'
result : a T (depends on WHAT is being defined)

Examples: 

cl-prompt> (def function fact (x) (if (zerop x) 1 (* x (fact (1- x)))))
FACT
 
cl-prompt> (def macro foo (y) `(list ,y 42))
FOO
 
cl-prompt> (def method bar :before ((x symbol)) ...)
#<STANDARD-METHOD bar (:BEFORE) (SYMBOL) 200A9D33>

;;; When WHAT is the symbol 'definer' the form has the following
;;; syntax:

def definer what (&key type-of-naming-form name body-name) &body definer-forms

;;; WHAT is a symbol or a cons. TYPE-OF-NAMING-FORM the symbol
;;; 'symbol', or the symbol 'cons' (or some other type specifier); the
;;; default is 'symbol'. NAME a symbol that is used within the body
;;; DEFINER-FORMS to retrieve the name of the object being defined;
;;; the default is the symbol 'name'. BODY-NAME a symbol that is used
;;; within the body DEFINER-FORMS to retrieve the actual definition
;;; forms for the definer; the default is the symbol
;;; 'definition-forms'. DEFINER-FORMS is a compound form.
;;;
;;; In this case the DEF macro behaves as follows. A
;;; BUILD-DEFINITION-FORM method on NAME is defined. The keyword
;;; parameters that can be passed to the DEF are meant to ease the
;;; writing of DEFINER-FORMS.

;;; TYPE-OF-NAMING-FORM is used in a DECLARE form to aid the compiler.
;;; NAME-VAR is a symbol that is used within the body DEFINER-FORMS to
;;; retrieve the name of the object being defined.

Side Effects:

NAME is defined as a WHAT in the environment. 'WHAT' is defined (pun
intended!) depends on the current set of methods for the generic
function BUILD-DEFINITION-FORM.
"
  (build-definition-form what name definition-forms))


;;; Basic definition forms for functions, methods and macros.
;;; Now you say:
;;;
;;;	(def function foo (x) (+ x 42))
;;;
;;; and
;;;
;;;	(def macro foo (y) `(list ,y 42))
;;;
;;; and
;;;
;;;	(def generic bar (x) (:method ((x string)) ...))
;;;
;;; and
;;;
;;;	(def method bar :before ((x symbol)) ...)

(defmethod build-definition-form ((what (eql 'function))
                                  (name symbol)
                                  definition-forms
                                  &optional prefix-options)
  `(defun ,name ,@definition-forms))


(defmethod build-definition-form ((what (eql 'macro))
                                  (name symbol)
                                  definition-forms
                                  &optional prefix-options)
  `(defmacro ,name ,@definition-forms))


(defmethod build-definition-form ((what (eql 'generic))
                                  (name symbol)
                                  definition-forms
                                  &optional prefix-options)
  `(defun ,name ,@definition-forms))


(defmethod build-definition-form ((what (eql 'method))
                                  (name symbol)
                                  definition-forms
                                  &optional prefix-options)
  (if (listp (first definition-forms))
      `(defmethod ,name ,@definition-forms)
      (let ((arglist-pos (position-if #'listp definition-forms)))
        (if arglist-pos
            `(defmethod ,name
                        ,@(subseq definition-forms 0 arglist-pos)
                        ,@(subseq definition-forms arglist-pos))
            (error 'program-error)))))


;;; Classes
;;; Now you say:
;;;
;;;	(def class reptilian (animal) ...)

(defmethod build-definition-form ((what (eql 'class))
                                  (name symbol)
                                  definition-forms
                                  &optional prefix-options)
  `(defclass ,name ,@definition-forms))


;;; Variables, parameters and constants.
;;; Now you say:
;;;
;;;	(def var *x*)
;;;
;;; and
;;;
;;;	(def parameter *foo* 42)
;;; and
;;;
;;;	(def constant +bar+ "bar")

(defmethod build-definition-form ((what (eql 'var))
                                  (name symbol)
                                  definition-forms
                                  &optional prefix-options)
  `(defvar ,name ,@definition-forms))


(defmethod build-definition-form ((what (eql 'parameter))
                                  (name symbol)
                                  definition-forms
                                  &optional prefix-options)
  `(defparameter ,name ,@definition-forms))


(defmethod build-definition-form ((what (eql 'constant))
                                  (name symbol)
                                  definition-forms
                                  &optional prefix-options)
  `(defconstant ,name ,@definition-forms))


;;; Structures.
;;; These are the straightforward definitions.
#|
(defmethod build-definition-form ((what (eql 'structure))
                                  (name symbol)
                                  definition-forms
                                  &optional prefix-options)
  `(defstruct ,name ,@definition-forms))


(defmethod build-definition-form ((what (eql 'structure))
                                  (name cons)
                                  definition-forms
                                  &optional prefix-options)
  `(defstruct ,name ,@definition-forms))
|#
;;; ... but this is more interesting: let's have a similar syntax for
;;; Classes and Structures.
;;; Now you say:
;;;
;;;	(def structure foo (a (s 42 :type number))
;;;	     (:documentation "Foo structure")
;;;	     (:print-function print-foo))

(defmethod build-definition-form ((what (eql 'structure))
                                  (name symbol)
                                  definition-forms
                                  &optional prefix-options)
  (destructuring-bind (slots &rest options)
      definition-forms
    (if options
        ;; The standard DEFSTRUCT options, but :DOCUMENTATION is
        ;; special.
        (let ((doc (find :documentation options :key #'first))
              (opts (remove :documentation options :key #'first))
              )
          `(defstruct (,name ,@opts) ,@(when doc (list (second doc))) ,@slots))
        `(defstruct ,name ,@slots))))


;;; Now let's define a meta "def" processor.
;;; The EVAL-WHEN is needed to ensure that the definition is ready for
;;; the compiler to compile the forms following this one.

(eval-when (:compile-toplevel :execute :load-toplevel)
  (defmethod build-definition-form ((what (eql 'definer))
                                    (for-what symbol)
                                    definition-forms
                                    &optional prefix-options)
    (destructuring-bind ((&key
                          ((:type-of-naming-form name-type) 'symbol) ; Non evaluated
                          ((:name name-var) 'name)                   ; Non evaluated
                          ((:body-name def-form-var) 'definition-forms) ; Non evaluated
                          )
                         &body definer-forms)
        definition-forms
      `(defmethod build-definition-form ((what (eql ',for-what))
                                         (,name-var ,name-type)
                                         ,def-form-var
                                         &optional prefix-options)
         ,@definer-forms)))
  )


;;; ... and here is a couple of (real!) example.


;;; Types.
;;; Now you say:
;;;
;;;	(def type buffer (x) `(vector character ,x))

(def definer type (:name type-name)
     `(deftype ,type-name ,@definition-forms))


;;; Packages.
;;; Now you say:
;;;
;;;     (def package "FOO" (:use "CL") (:export "BAR"))

(def definer package (:name package-name)
     `(defpackage ,package-name ,@definition-forms))


;;; Conditions.
;;; Now you say:
;;;
;;;	(def condition my-error (simple-error) ...)

(def definer condition (:name condition-name :body-name cnd-def-body)
     `(define-condition ,condition-name ,@cnd-def-body))


;;; Method Combinations.
;;; Now you say:
;;;
;;;	(def method-combination my-error (simple-error) ...)
;;;
;;; Works for both "short" and "long" forms.

(def definer method-combination (:name mcn :body-name mc-def-body)
     `(define-method-combination ,mcn ,@mc-def-body))


;;; SETF
;;; Now you say:
;;;
;;;	(def setf middleguy set-middleguy)
;;;
;;; Works for both "short" and "long" forms.

(def definer setf (:name accessor-fn :body-name defsetf-def-body)
     `(defsetf ,accessor-fn ,@defsetf-def-body))


;;; SETF-EXPANDER
;;; Now you say:
;;;
;;;	(def setf-expander lastguy (x &environment env) #|...|#)
;;;
;;; Works for both "short" and "long" forms.

(def definer setf-expander (:name accessor-fn :body-name setfexp-def-body)
     `(define-setf-expander ,accessor-fn ,@setfexp-def-body))


;;; MODIFY-MACRO
;;; Now you say:
;;;
;;;	(def modify-macro appendf (&rest args) 
;;;          append "Append onto list")
;;;
;;; Works for both "short" and "long" forms.

(def definer modify-macro (:name mm-name :body-name mm-def-body)
     `(define-modify-macro ,mm-name ,@mm-def-body))


;;; Finally, lets define something that will allow us to write
;;;
;;; (def (inline (values (vector double-float) symbol) function) foo (x y) ...)
;;;
;;; The only requirement is that the last element of the list is the
;;; discriminant. The prefix arguments are passed in the
;;; PREFIX-OPTIONS parameter.

(defmethod build-definition-form ((what list)
                                  (name t)
                                  definition-forms
                                  &optional prefix-options)
  (build-definition-form (first (last what))
                         name
                         definition-forms
                         (butlast what)))

;;; ... and here is the kicker.

(defun type-specifier-p (x)
  (nth-value 0 (ignore-errors (subtypep x t))))


#|
;;; The next ones are commented because I am experimenting with different
;;; syntax.

(defmethod build-definition-form :around ((what (eql 'function))
                                          (name symbol)
                                          definition-forms
                                          &optional prefix-options)
  (if prefix-options
      (let ((inlinep (find 'inline prefix-options))
            (return-type (find-if #'type-specifier-p prefix-options))
            )
        (declare (ignorable return-type))
        `(progn
           ,@(when inlinep `((declaim (inline ,name))))
           ;; Here we could build the (declaim (ftype ...)) form by
           ;; munging the lambda list and the definition body.
           ;; We ignore it for the time being.
           ,(call-next-method)))
      (call-next-method)))


(defmethod build-definition-form :around ((what (eql 'function))
                                          (name cons)  ; (setf <name>)
                                          definition-forms
                                          &optional prefix-options)
  (cond ((and (= (length name) 2) (eql (first name) 'setf))
         ;; (setf <name>)
         (if prefix-options
             (let ((inlinep (find 'inline prefix-options))
                   (return-type (find-if #'type-specifier-p prefix-options))
                   )
               (declare (ignorable return-type))
               `(progn
                  ,@(when inlinep `((declaim (inline ,name))))
                  ;; Here we could build the (declaim (ftype ...)) form by
                  ;; munging the lambda list and the definition body.
                  ;; We ignore it for the time being.
                  ,(call-next-method)))
             (call-next-method)))
        (t (error 'program-error))))


;;; Now you say
;;;
;;;	(def (inline fixnum function) foo (x) (mod (1+ x) most-positive-fixnum))


;;; Alternative 1.
;;; Of course the DEF macro could be defined in such a way to achieve
;;; a different effect.  A new syntax like:
;;;
;;;	(def function (foo :inline t :values (fixnum double)) (x (y integer) z) ...)
;;;
;;; may be better (and more similar to DEF STRUCTURE.)
;;;
;;; Alternative 2.
;;; Couple the return type with the function name.
;;;
;;;	(def (inline function) (foo (values fixnum double)) (x (y integer) z) ...)
;;;
;;; may be the best, but it may not be so attractive.

|#

#|
Of course, things can get more interesting.
E.g in UFFI you'd write

	(uffi:def-foreign-type char-ptr (* :char))

Now you can write

	(def uffi:foreign-type char-ptr (* :char))

|#

;;; end of file -- definer.lisp --
